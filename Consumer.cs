﻿using System;
using System.Threading.Tasks;

namespace Queuing
{
    public class Consumer<T, TResult> where T : new()
    {

        public bool ProcessingQueue { get; private set; }

        private Func<T, TResult> Job;

        public bool Busy { get; private set; } = false;

        public event Action<T, TResult> AfterConsume;

        public Consumer(Func<T, TResult> ConsumeFunc)
        {
            Job = ConsumeFunc;
        }

        public async Task Consume(T Item)
        {
            Busy = true;
            try
            {
                await Task.Run(() =>
                {
                    var result = Job(Item);
                    AfterConsume?.Invoke(Item, result);
                });
            }
            finally
            { 
                Busy = false; 
            }
        }

        public async void ProcessQueue(BlockingQueue<T> queue)
        {
            try
            {
                await Task.Run(async () =>
                {
                    while (queue.TryDequeue(out T elem))
                        await Consume(elem);
                });
            }
            finally
            {
                ProcessingQueue = false; 
            }
        }
    }

}
