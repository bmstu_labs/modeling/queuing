﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Queuing
{
    public class Producer<TResult> where TResult : new()
    {
        private Func<TResult> Job;

        private bool _stopping;

        public bool Running { get; private set; } = false;

        public event Action<TResult> OnProduce;

        public Producer(Func<TResult> ProduceFunc)
        {
            Job = ProduceFunc;
        }

        public async void Produce()
        {
            Running = true;

            await Task.Run(() =>
            {
                while (!_stopping)
                {
                    TResult obj = Job();
                    OnProduce?.Invoke(obj);
                }
                _stopping = false;
            });

            Running = false;
        }

        public void Stop()
        {
            _stopping = Running;
        }
    }
}   
