﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Queuing
{

    public class BlockingQueue<T> where T : new()
    {
        int _capacity;

        public event Action<int> CapacityChanged;

        public bool AutoLength { get; set; } = false;


        private bool closing;

        private readonly Queue<T> queue;

        public BlockingQueue(int Capacity)
        {
            _capacity = Capacity;

            queue = new Queue<T>(Capacity);

            lock (queue)
            {
                closing = false;
                Monitor.PulseAll(queue);
            }
        }

        public int Capacity
        {
            get => _capacity;
            set
            {
                _capacity = value;
                CapacityChanged?.Invoke(_capacity);
            }
        }

        public int Count
        {
            get
            {
                lock (queue)
                {
                    return queue.Count;
                }
            }
        }

        public bool Enqueue(T item)
        {
            if (Count == Capacity)
                if (!AutoLength)
                    throw new OverflowException("Очередь переполнена");
                else
                    Capacity++;

            lock (queue)
            {
                if (closing || null == item)
                    return false;

                queue.Enqueue(item);

                if (queue.Count == 1)
                    Monitor.PulseAll(queue); // wake up any blocked dequeue

                return true;
            }
        }

        public void Close()
        {
            lock (queue)
            {
                if (!closing)
                {
                    closing = true;
                    queue.Clear();
                    Monitor.PulseAll(queue);
                }
            }
        }

        public bool TryDequeue(out T value, int timeout = Timeout.Infinite)
        {
            lock (queue)
            {
                while (queue.Count == 0)
                {
                    if (closing || (timeout < Timeout.Infinite) || !Monitor.Wait(queue, timeout))
                    {
                        value = default(T);
                        return false;
                    }
                }

                value = queue.Dequeue();
                return true;
            }
        }

        public void Clear()
        {
            lock (queue)
            {
                queue.Clear();
                Monitor.Pulse(queue);
            }
        }
    }
}