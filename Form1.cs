﻿using Randoms;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Queuing
{
    public partial class Form1 : Form
    {
        BlockingQueue<Queuing.Message> queue;
        Producer<Message> producer;
        Consumer<Message, bool> consumer;

        int Successes;
        int Faults;

        public int MinProduceTime { get; set; } = 500;
        public int MaxProduceTime { get; set; } = 500;

        public int MeanProcessTime { get; set; } = 2500;

        private readonly Random rnd = new Random();

        public Form1()
        {
            InitializeComponent();
            timer1.Interval = (int)numericUpDown1.Value;
            chart1.Titles.Add("Событийный подход");
            chart2.Titles.Add("Δt подход");
            
            queue = new BlockingQueue<Message>((int)udQueueLen.Value);

            producer = new Producer<Message>(() =>
            {
                Thread.Sleep(rnd.Next(MinProduceTime, MaxProduceTime));
                return new Message();
            });

            consumer = new Consumer<Message, bool>(
                (Message m) =>
                {
                    Thread.Sleep(rndPoisson.GetNext((MeanProcessTime / 1000) * 1000));
                    return true;
                });

            udMaxTime.Maximum = udMinTime.Value;

            queue.CapacityChanged += queue_CapacityChanged;
            producer.OnProduce += producer_OnProduceMessage;
            consumer.AfterConsume += consumer_AfterConsume;
        }

        public void ClearChart()
        {
            foreach (var s in chart1.Series)
                s.Points.Clear();
        }

        public void refreshChart(Chart chart)
        {
            if (producer.Running)
            {
                chart.Series[0].Points.Add(queue.Count);
                chart.Series[1].Points.Add(Successes);
                chart.Series[2].Points.Add(Faults);
                chart.Series[3].Points.Add(queue.Capacity);
            }
        }

        private void producer_OnProduceMessage(Message m)
        {
            try
            {
                Invoke(new Action(() =>
                {
                    try
                    {
                        queue.Enqueue(m);
                    }
                    catch (OverflowException)
                    {
                        Faults++;
                    }
                    refreshChart(chart1);
                }));
            }
            catch (ObjectDisposedException) { }
        }

        private void queue_CapacityChanged(int new_capacity)
        {
            try
            {
                Invoke(new Action(() =>
                {
                    udQueueLen.Value = new_capacity;
                }));
            }
            catch (ObjectDisposedException) { }
        }

        private void consumer_AfterConsume(Message m, bool Result)
        {
            try
            {
                Successes++;
                Invoke(new Action(() => refreshChart(chart1)));
            }
            catch (ObjectDisposedException) { }
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            if (producer.Running)
            {
                producer.Stop();
                button1.Text = "Начать моделирование";
                queue.Clear();
                timer1.Stop();
            }
            else
            {
                button1.Text = "Остановить моделирование";
                Successes = 0;
                Faults = 0;
                ClearChart();
                refreshChart(chart2);
                timer1.Start();

                await Task.Run(() =>
                {
                    DateTime startTime = DateTime.Now;
                    
                    producer.Produce();
                    consumer.ProcessQueue(queue);

                    while (producer.Running)
                        ;
                });
            }
        }

        private void QueueAutoLen_Click(object sender, EventArgs e)
        {
            udQueueLen.Enabled = !QueueAutoLen.Checked;
            queue.AutoLength = QueueAutoLen.Checked;
        }

        private void udQueueLen_ValueChanged(object sender, EventArgs e)
        {
            if ((queue != null) && (queue.Capacity != (int)udQueueLen.Value))
                queue.Capacity = (int)udQueueLen.Value;
        }

        private void udMaxTime_ValueChanged(object sender, EventArgs e)
        {
            udMinTime.Maximum = udMaxTime.Value;
            
            if (udMinTime.Value > udMinTime.Maximum)
                udMinTime.Value = udMaxTime.Value;

            MaxProduceTime = (int)udMaxTime.Value;
        }

        private void udMinTime_ValueChanged(object sender, EventArgs e)
        {
            udMaxTime.Minimum = udMinTime.Value;

            if (udMaxTime.Value < udMaxTime.Minimum)
                udMaxTime.Value = udMaxTime.Value;

            MinProduceTime = (int)udMinTime.Value;
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            chart1.Height = ClientRectangle.Height / 2;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Invoke(new Action(() => refreshChart(chart2)));
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            timer1.Interval = (int)numericUpDown1.Value;
        }
    }
}