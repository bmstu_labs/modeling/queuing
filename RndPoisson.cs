﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Randoms
{
    public class rndPoisson
    {
        private static Random rnd;
        static rndPoisson()
        {
            rnd = new Random();
        }
        public static int GetNext(double lambda)
        {
            double L = Math.Exp(-lambda);
            double p = 1.0;
            int k = 0;

            do
            {
                k++;
                p *= rnd.NextDouble();
            } while (p > L);

            return k - 1;
        }
    }
}
