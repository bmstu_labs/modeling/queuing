﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Queuing
{
    public partial class Form2 : Form
    {
        Producer<Message> Clients;
        
        Consumer<Message, bool>[] Operators = new Consumer<Message, bool>[3];
        
        (int MinTime, int MaxTime)[] operatorTimes =
        {
            (15,25),
            (30,50),
            (20,60),
        };

        private int boost = 100;

        public int Boost { get => boost; set => boost = value; }

        int Requests = 0;
        int Successes = 0;
        int Faults = 0;
        int op_runing_qnt = 0;

        private readonly Random rnd = new Random();

        public Form2()
        {
            InitializeComponent();
            
            chart1.Legends.Clear();
            chart3.Legends.Clear();

            chart2.Legends[0].Docking = Docking.Bottom;

            int k = 0;
            foreach (var s in new string[]
                { "Тек. загрузка", "Кол-во заявок", "Обслужено", "Отказано", "% отказов", "Ускорение"})
            {
                dataGridView1.Rows.Add(new DataGridViewRow());
                dataGridView1.Rows[k].Cells[0].Value = s;
                k++;
            }

            button1.Click += button1_Click;
            button2.Click += Button2_Click;

            Clients = new Producer<Message>(
                () => {
                    Thread.Sleep(rnd.Next(8 * (int)((100D / Boost) * 100), 12 * (int)((100D / Boost) * 100)));
                    return new Message();
                });


            for (int i = 0; i < 3; i++)
            {
                int local_i = i;
                Operators[i] = new Consumer<Message, bool>
                (
                    (Message m) =>
                    {
                        Thread.Sleep(rnd.Next(operatorTimes[local_i].MinTime * (int)((100D / Boost) * 100), operatorTimes[local_i].MaxTime * (int)((100D / Boost) * 100)));
                        return true;
                    }
                );
                Operators[i].AfterConsume += operators_AfterConsume;
            }

            Clients.OnProduce += clients_OnProduce;

            FormClosing += (object sender, FormClosingEventArgs e) =>
            {
                for (int i = 0; i < 3; i++)
                    Operators[i].AfterConsume -= operators_AfterConsume;
                Clients.OnProduce -= clients_OnProduce;
            };
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            if (Clients.Running)
                Clients.Stop();

            button1.Text = "Начать моделирование";

            Reset();
        }

        private void operators_AfterConsume(Message m, bool Result)
        {
            try
            {
                Invoke(new Action(() =>
                {
                    Successes++;
                    op_runing_qnt--;
                    refreshChart();
                }));
            }
            catch (ObjectDisposedException) { }
        }

        private void clients_OnProduce(Message m)
        {
            try
            {
                Invoke(new Action(() =>
                {
                    Requests++;
                    bool Managed = false;
                    for (int i = 0; i < 3; i++)
                        if (!Operators[i].Busy)
                        {
                            op_runing_qnt++;
                            _ = Operators[i].Consume(m);
                            Managed = true;
                            break;
                        }

                    if (!Managed)
                        Faults++;

                    refreshChart();
                }));
            }
            catch (ObjectDisposedException) { }
        }

        public void Reset()
        {   
            foreach (var s in chart1.Series)
                s.Points.Clear();
            foreach (var s in chart2.Series)
                s.Points.Clear();
            foreach (var s in chart3.Series)
                s.Points.Clear();

            Faults = 0;
            Successes = 0;
            Requests = 0;
        }

        public void refreshChart()
        {
            for (int i = 0; i < 3; i++)
                chart1.Series[i].Points.Add(i- op_runing_qnt < 0 ? 1 : 0);

            chart2.Series[0].Points.Add(Successes);
            chart2.Series[1].Points.Add(Faults);

            for (int i = 0; i < 3; i++)
            {
                chart3.Series[i].Points.Clear();
                chart3.Series[i].Points.Add(i - op_runing_qnt < 0 ? 1 : 0);
            }

            dataGridView1.Rows[0].Cells[1].Value = op_runing_qnt.ToString();
            dataGridView1.Rows[1].Cells[1].Value = Requests.ToString();
            dataGridView1.Rows[2].Cells[1].Value = Successes;
            dataGridView1.Rows[3].Cells[1].Value = Faults;
            dataGridView1.Rows[4].Cells[1].Value = (Successes + Faults) == 0 ? "0" : Math.Round(((double)Faults / (Successes+Faults))*100).ToString();
            dataGridView1.Rows[5].Cells[1].Value = Boost.ToString();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            if (Clients.Running)
            {
                Clients.Stop();
                button1.Text = "Продолжить моделирование";
            }
            else
            {
                button1.Text = "Остановить моделирование";                

                await Task.Run(() =>
                {
                    DateTime startTime = DateTime.Now;

                    Clients.Produce();

                    while (Clients.Running)
                        ;
                });
            }
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            Boost = trackBar1.Value * 100;
            dataGridView1.Rows[5].Cells[1].Value = Boost.ToString();
        }
    }
}